package com.maxxi.intern.project2.repositories;

import com.maxxi.intern.project2.repositories.entities.Boundary;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BoundaryRepository extends JpaRepository<Boundary, String> {
}
