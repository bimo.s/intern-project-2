package com.maxxi.intern.project2.repositories;

import com.maxxi.intern.project2.repositories.entities.ObstacleZones;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ObstacleZonesRepository extends JpaRepository<ObstacleZones, String> {

}
