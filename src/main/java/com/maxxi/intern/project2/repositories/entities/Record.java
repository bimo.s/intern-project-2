package com.maxxi.intern.project2.repositories.entities;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;


@Entity
@Table(name = "record")
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Record extends BaseEntity{

    @Column(name = "record_id")
    private String recordId;

    @Column(name = "drone_id")
    private String droneId;

    @Column(name = "username")
    private String username;

    @Column(name = "take_off_time")
    private ZonedDateTime takeOffTime;

    @Column(name = "landed_time")
    private ZonedDateTime landedTime;

    @Column(name = "work_type")
    private String workType;

    @Column(name = "work_area")
    private Double workArea;

    @Column(name = "work_amount")
    private Double workAmount;

    @Column(name = "take_off_point")
    private String takeOffPoint;

    @Column(name = "landed_point")
    private String landedPoint;

    @OneToMany(mappedBy = "record")
    private List<Track> trackList = new ArrayList<>();

}
