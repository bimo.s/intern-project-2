package com.maxxi.intern.project2.repositories.entities;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "coordinate")
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Coordinate extends BaseEntity {
    @Column(name = "lat")
    private Double lat;

    @Column(name = "lng")
    private Double lng;

    @ManyToOne
    @JoinColumn(name = "obstacle_zones_id")
    private ObstacleZones obstacleZones;
}
