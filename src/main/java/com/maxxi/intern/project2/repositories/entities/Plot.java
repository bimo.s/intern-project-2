package com.maxxi.intern.project2.repositories.entities;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import java.text.DateFormat;
import java.time.ZonedDateTime;
import java.util.Date;

@Entity
@Table(name = "plot", uniqueConstraints = {@UniqueConstraint(columnNames = {"plot_id"})})
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Plot extends BaseEntity{

    @Column(name = "plot_id", unique = true)
    private String plotId;

    @Column(name = "plot_name")
    private String plotName;

    @Column(name = "plot_area")
    private Integer plotArea;

    @Column(name = "username")
    private String username;

    @Column(name = "create_time")
    private ZonedDateTime createTime;

}
