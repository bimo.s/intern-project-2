package com.maxxi.intern.project2.repositories;

import com.maxxi.intern.project2.repositories.entities.Track;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TrackRepository extends JpaRepository<Track, String> {
}
