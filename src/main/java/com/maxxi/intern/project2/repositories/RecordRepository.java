package com.maxxi.intern.project2.repositories;

import com.maxxi.intern.project2.repositories.entities.Record;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RecordRepository extends JpaRepository<Record, String> {
}
