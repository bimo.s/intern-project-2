package com.maxxi.intern.project2.repositories.entities;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "obstacle_zones")
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ObstacleZones extends BaseEntity {
    @ManyToOne
    @JoinColumn(name = "plot_id")
    private Plot plot;
}
