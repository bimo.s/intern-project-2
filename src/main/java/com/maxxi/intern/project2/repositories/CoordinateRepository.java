package com.maxxi.intern.project2.repositories;

import com.maxxi.intern.project2.repositories.entities.Coordinate;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CoordinateRepository extends JpaRepository<Coordinate,String> {
}
