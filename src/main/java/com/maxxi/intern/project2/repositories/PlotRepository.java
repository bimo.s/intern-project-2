package com.maxxi.intern.project2.repositories;

import com.maxxi.intern.project2.repositories.entities.Plot;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PlotRepository extends JpaRepository<Plot, String> {

    Plot findByPlotId(String plotId);
}
