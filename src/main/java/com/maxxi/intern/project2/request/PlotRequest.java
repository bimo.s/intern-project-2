package com.maxxi.intern.project2.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import com.maxxi.intern.project2.repositories.entities.Coordinate;
import com.maxxi.intern.project2.repositories.entities.ObstacleZones;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
public class PlotRequest {
    @NotNull
    private  String plotId;

    @NotNull
    private String plotName;

    @NotNull
    private Integer plotArea;

    @NotNull
    private String username;

    @NotNull
    private Integer createTime;

    @NotNull
    private List<List<CoordinateRequest>> obstacleZones;

    @NotNull
    private List<CoordinateRequest> boundary;
}
