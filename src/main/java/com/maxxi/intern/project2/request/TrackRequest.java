package com.maxxi.intern.project2.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class TrackRequest {
    @JsonProperty("lng")
    private Double lng;
    @JsonProperty("lat")
    private Double lat;
    @JsonProperty("time")
    private long time;

}
