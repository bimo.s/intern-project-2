package com.maxxi.intern.project2.services.v1.impl;

import com.maxxi.intern.project2.repositories.RecordRepository;
import com.maxxi.intern.project2.repositories.TrackRepository;
import com.maxxi.intern.project2.repositories.entities.Record;
import com.maxxi.intern.project2.repositories.entities.Track;
import com.maxxi.intern.project2.request.RecordRequest;
import com.maxxi.intern.project2.request.TrackRequest;
import com.maxxi.intern.project2.services.v1.RecordService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
@RequiredArgsConstructor
public class RecordServiceImpl implements RecordService {

    private final RecordRepository recordRepository;
    private final TrackRepository trackRepository;
    @Override
    public String addRecord(RecordRequest recordRequest) {
        ZonedDateTime takeOffTime = Instant.ofEpochMilli(recordRequest.getTakeOffTime()+'L').atZone(ZoneOffset.UTC);
        ZonedDateTime landedOffTime = Instant.ofEpochMilli(recordRequest.getLandedTime()+'L').atZone(ZoneOffset.UTC);
        Record newRecord = Record.builder()
                .recordId(recordRequest.getRecordId())
                .droneId(recordRequest.getDroneId())
                .username(recordRequest.getUsername())
                .takeOffTime(takeOffTime)
                .landedTime(landedOffTime)
                .workType(recordRequest.getWorkType())
                .workArea(recordRequest.getWorkArea())
                .workAmount(recordRequest.getWorkAmount())
                .takeOffPoint(recordRequest.getTakeOffPoint())
                .landedPoint(recordRequest.getLandedPoint())
                .build();
                newRecord.setCreatedBy("SYSTEM");

        Record saveRecord = recordRepository.save(newRecord);

        List<Track> tracks = new ArrayList<>();
        for (int i = 0; i < recordRequest.getTrack().size(); i++){
            TrackRequest trackRequest = recordRequest.getTrack().get(i);
            ZonedDateTime time =Instant.ofEpochMilli(trackRequest.getTime() + 'L').atZone(ZoneOffset.UTC);
            Track track = Track.builder()
                    .lat(trackRequest.getLat())
                    .lng(trackRequest.getLng())
                    .time(ZonedDateTime.parse(String.valueOf(time)))
                    .build();
            track.setCreatedBy("SYSTEM");
            track.setRecord(saveRecord);
            track.setCreatedDate(new Date());
            tracks.add(track);
        }
        trackRepository.saveAll(tracks);
        return "success";
    }
}
