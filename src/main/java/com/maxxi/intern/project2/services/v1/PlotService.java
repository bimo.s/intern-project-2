package com.maxxi.intern.project2.services.v1;

import com.maxxi.intern.project2.request.PlotRequest;
import com.maxxi.intern.project2.responses.BaseResponse;

import java.util.List;

public interface PlotService {

    String addPlot(PlotRequest plotRequest);
}
