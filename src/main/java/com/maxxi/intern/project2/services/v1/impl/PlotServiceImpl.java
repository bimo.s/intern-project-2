package com.maxxi.intern.project2.services.v1.impl;

import com.maxxi.intern.project2.repositories.BoundaryRepository;
import com.maxxi.intern.project2.repositories.CoordinateRepository;
import com.maxxi.intern.project2.repositories.ObstacleZonesRepository;
import com.maxxi.intern.project2.repositories.PlotRepository;
import com.maxxi.intern.project2.repositories.entities.Boundary;
import com.maxxi.intern.project2.repositories.entities.Coordinate;
import com.maxxi.intern.project2.repositories.entities.ObstacleZones;
import com.maxxi.intern.project2.repositories.entities.Plot;
import com.maxxi.intern.project2.request.CoordinateRequest;
import com.maxxi.intern.project2.request.PlotRequest;
import com.maxxi.intern.project2.services.v1.PlotService;
import com.maxxi.intern.project2.utils.exceptions.BadRequestException;
import lombok.RequiredArgsConstructor;
import net.bytebuddy.implementation.bytecode.Throw;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class PlotServiceImpl implements PlotService {

    private final PlotRepository plotRepository;
    private final BoundaryRepository boundaryRepository;
    private final ObstacleZonesRepository obstacleZonesRepository;
    private final CoordinateRepository coordinateRepository;

    @Override
    public String addPlot(PlotRequest plotRequest) {
        Plot plotId = plotRepository.findByPlotId(plotRequest.getPlotId());
        if (plotId!=null){
            throw new BadRequestException("PlotId already exists!");
        }
        ZonedDateTime utc = Instant.ofEpochMilli(plotRequest.getCreateTime()+'L').atZone(ZoneOffset.UTC);
        Plot plot = Plot.builder()
                .plotId(plotRequest.getPlotId())
                .plotName(plotRequest.getPlotName())
                .plotArea(plotRequest.getPlotArea())
                .username(plotRequest.getUsername())
                .createTime(ZonedDateTime.parse(String.valueOf(utc)))
                .build();
        plot.setCreatedBy("SYSTEM");
        plotRepository.save(plot);

        for (List<CoordinateRequest> obstacleZonesList : plotRequest.getObstacleZones()) {
            ObstacleZones obstacleZone = new ObstacleZones();
            obstacleZone.setPlot(plot);
            obstacleZone.setCreatedBy("SYSTEM");
            ObstacleZones obstacleZones = obstacleZonesRepository.save(obstacleZone);
            List <Coordinate> coordinates = new ArrayList<>();
            for (CoordinateRequest coordinateRequest : obstacleZonesList) {
                Coordinate coordinate = Coordinate.builder()
                        .lat(coordinateRequest.getLat())
                        .lng((coordinateRequest.getLng()))
                        .build();
                coordinate.setObstacleZones(obstacleZones);
                coordinate.setCreatedBy("SYSTEM");
                coordinates.add(coordinate);
            }
            coordinateRepository.saveAll(coordinates);
        }
        List<Boundary> boundaryList = new ArrayList<>();
        for (CoordinateRequest coordinateRequest : plotRequest.getBoundary()) {
            Boundary boundary = Boundary.builder()
                    .lat(coordinateRequest.getLat())
                    .lng(coordinateRequest.getLng())
                    .build();
            boundary.setPlot(plot);
            boundary.setCreatedBy("SYSTEM");
            boundaryList.add(boundary);
        }
        boundaryRepository.saveAll(boundaryList);
        return "Created";


    }

}
