package com.maxxi.intern.project2.services.v1;

import com.maxxi.intern.project2.request.RecordRequest;

public interface RecordService {
    String addRecord(RecordRequest recordRequest);
}
