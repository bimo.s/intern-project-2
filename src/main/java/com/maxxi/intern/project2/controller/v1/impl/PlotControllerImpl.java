package com.maxxi.intern.project2.controller.v1.impl;

import com.maxxi.intern.project2.controller.v1.PlotController;
import com.maxxi.intern.project2.request.PlotRequest;
import com.maxxi.intern.project2.responses.BaseResponse;
import com.maxxi.intern.project2.services.v1.PlotService;
import com.maxxi.intern.project2.utils.ResponseHelper;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
public class PlotControllerImpl implements PlotController {

    private final PlotService plotService;
    @Override
    public ResponseEntity<BaseResponse> savePlot(PlotRequest plotRequest) {
        return ResponseHelper.buildOkResponse(plotService.addPlot(plotRequest));
    }
}
