package com.maxxi.intern.project2.controller.v1;

import com.maxxi.intern.project2.request.PlotRequest;
import com.maxxi.intern.project2.responses.BaseResponse;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("api/v1/plot")
public interface PlotController {

    @PostMapping
    ResponseEntity<BaseResponse> savePlot(@RequestBody PlotRequest plotRequest);
}
