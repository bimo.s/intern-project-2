package com.maxxi.intern.project2.controller.v1.impl;

import com.maxxi.intern.project2.controller.v1.RecordController;
import com.maxxi.intern.project2.request.RecordRequest;
import com.maxxi.intern.project2.responses.BaseResponse;
import com.maxxi.intern.project2.services.v1.RecordService;
import com.maxxi.intern.project2.utils.ResponseHelper;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
@RequiredArgsConstructor
public class RecordControllerImpl implements RecordController {

    private final RecordService recordService;

    @Override
    public ResponseEntity<BaseResponse> addRecord(RecordRequest recordRequest) {
        return ResponseHelper.buildOkResponse(recordService.addRecord(recordRequest));
    }
}
