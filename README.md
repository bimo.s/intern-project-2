# Intern Project
<!--- These are examples. See https://shields.io for others or to customize this set of shields. You might want to include dependencies, project status and licence info here --->
![Java](https://img.shields.io/badge/java-%23ED8B00.svg?style=for-the-badge&logo=java&logoColor=white)
![IntelliJ IDEA](https://img.shields.io/badge/IntelliJIDEA-000000.svg?style=for-the-badge&logo=intellij-idea&logoColor=white)

## Prerequisites

Before you begin, ensure you have met the following requirements:
* You have installed the Java Version 11
* You have a `<Windows/Linux/Mac>` machine.
* You have read [Jira Work](https://maxxitani.atlassian.net/l/cp/wE2Eqs0w).


## Description project
a backend application development project by implementing a Restfull API which aims to manage information about drone activity Records (record) and Plots (plot). This API will allow users to perform basic Create operations on these entities via an HTTP POST request.

## Feature Project

| Record Api :  | Plot Api :  |
|---------------|-------------|
| Create Record | Create Plot |

## Request body requirements

Plot request
```
{
    "plot_id": "008e7ece5eab4e26a6032f936562e218",
    "plot_name": "p.m 10.26 a",
    "plot_area": 23,
    "username": "123@gmail.com",
    "create_time": 1643,
    "obstacle_zones": [
        [
            {
                "lat": 30.30789437023634,
                "lng": 120.37110628412005
            },
            {
                "lat": 30.30789437023634,
                "lng": 120.37110628412005
            },
            {
                "lat": 30.30789437023634,
                "lng": 120.37110628412005
            }
        ],
        [
            {
                "lat": 30.30789437023634,
                "lng": 120.37110628412005
            },
            {
                "lat": 30.30789437023634,
                "lng": 120.37110628412005
            },
            {
                "lat": 30.30789437023634,
                "lng": 120.37110628412005
            }
        ]
    ],
    "boundary": [
        {
            "lat": 30.30789437023634,
            "lng": 120.37110628412005
        },
        {
            "lat": 30.30789437023634,
            "lng": 120.37110628412005
        },
        {
            "lat": 30.30789437023634,
            "lng": 120.37110628412005
        },
        {
            "lat": 30.30789437023634,
            "lng": 120.37110628412005
        }
    ]
}
```

Record request 

```
{
    "recordId": "A22-21-01-001_1672502400000",
    "droneId": "A22-21-01-001",
    "username": "123@gmail.com",
    "workType": "spray",
    "takeOffTime": 1672502400000,
    "landedTime": 167250300000,
    "workArea": 1.55,
    "workAmount": 1.55,
    "takeOffPoint": "120.7652088,30.6554832",
    "landedPoint": "120.7652088,30.6554832",
    "track": [
        {
            "lng": 120.7652088,
            "lat": 30.6554832,
            "time": 167250300000
        }
    ]
}
```

## ERD (Entity Realtionship Diagram)

Plot ERD

![Alt Text](https://i.imgur.com/HqlqoWZ.jpeg)

Record ERD

![Alt Text](https://i.imgur.com/5Q8j9Ys.jpeg)


## Dependencies requirement

```
Spring Data JPA (SQL)
Spring Web
Spring Data JDBC
Lombok
Validation
Mysql Driver
```


## Installing Bitzer Dependencies

```
mvn clean install
```

Add run commands and examples you think users will find useful.
